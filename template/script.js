
function includeHTML() {
    var z, i, elmnt, file, xhttp;
    /*loop through a collection of all HTML elements:*/
    z = document.getElementsByTagName("*");
    for (i = 0; i < z.length; i++) {
      elmnt = z[i];
      /*search for elements with a certain atrribute:*/
      let file = elmnt.getAttribute("w3-include-html");
      if (file) {
        /*make an HTTP request using the attribute value as the file name:*/
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4) {
            console.log(this.status);
            if (this.status == 200) {elmnt.innerHTML = this.responseText;}
            if (this.status == 404) {elmnt.innerHTML = "Page not found.";}
            /*remove the attribute, and call this function once more:*/
            elmnt.removeAttribute("w3-include-html");
            includeHTML();
            headerColor();
            // if(file.indexOf("header")>-1) {
            //   setClick();
            //   // window.handleradded=true;
            // }
              
          }
        }      
        xhttp.open("GET", file, false);
        xhttp.send();
        /*exit the function:*/
        return;
      }
    }
  };
  

  function headerColor(){
    if(location.pathname.length<2 && location.host.indexOf("docs")==-1){
        document.getElementsByTagName("header")[0].classList.add("home-header");
    }
        
}
function opensidemenu(){
    $(".nav-holder").toggleClass("is-active");
    if($(".nav-holder.is-active").length>0){
        document.body.style.overflow="hidden";
    }
    else{
        document.body.style.overflow="auto";
    }
}

  includeHTML();