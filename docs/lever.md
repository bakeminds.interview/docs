## HulkHire < > Lever Integration

Loosing on Right talent does not benefit anyone. Neither companies nor talent.  
We are here to ensure, it never happens.

Standardising the interview process is a most crucial part of hiring to ensure all candidates are treated fairly and are getting enough chance to prove their potential. Resulting is closing positions like never before.

We are here to help you achieve exactly the same.​

### Why chose HulkHire for Technical Interviews?
​
​HulkHire is a technical interview platform that help accurately assess where the talent stands. Giving you 360 degree feedback about candidate. Our interview experts are professionally trained to conduct technical interviews. Off office hours availbility, Ensure a memorable interview experience, And end to end integration with ATS are just few of our best features that we offer.

Interview with HulkHire, and don't loose good talents again

### What this Integration will do? 
Lever is crucial and critical part of your hiring process.  
And we truly understand it. By integrating HulkHire with your lever account, You can send invitation of HulkHire interviews directly through Lever.  

So no more switching between portals, no need to remember additional credentials.
Get all interview updates in your own LEVER account.

​

### Setting up integration

#### Prerequisite
1. Active Account on Lever with Super Admin access
2. Active account on HulkHire with Admin access

#### Steps
1. Login to Lever account, Go to **Settings > Integrations and API > Webhooks**
2. Ensure Candidate State Change is enabled  

<img src="images/lever-webhook.png" style="margin:auto; display:block; width: 600px; max-width: 80vw;" alt="Lever Settings"/>


3. Login to HulkHire and Go to **Admin > Integrations**
4. Look for **Lever** panel and click on **Connect**  

<img src="images/hulkhire-connect.png" style="margin:auto; display:block; width: 600px; max-width: 80vw;" alt="HulkHire Connect"/>



5. It will redirect you to new page, asking your permission with required privileges


<img src="images/permissions.png" style="margin:auto; display:block; width: 200px; max-width: 80vw;" alt="HulkHire Permissions for Lever"/>



6. Click on **Accept** , It will redirect you HulkHire Integration Success page

<img src="images/intergration-success.png" style="margin:auto; display:block; width: 600px; max-width: 80vw;" alt="Lever HulkHire Integration Success"/>

<!-- ![Lever HulkHire Integration Success](images/integration-success.png) -->


7. ***Congratulations***, Integration is complete
​


**How to set HulkHire Interviews in Lever**

Setting up Interviews with HulkHire is very easy.
Once the candidate profile is moved to interview stages in Lever, An interview schedule link will be added to candidate page.


<img src="images/hulkhire-link.png" style="margin:auto; display:block; width: 600px; max-width: 80vw;" alt="Candidate HulkHire Interview Invite"/>

<!-- ![Candidate HulkHire Interview Invite](images/hulkhire-link.png){width=50%} -->


Click on the link, choose the interview you would like to schedule.
And Just click on send Invitation  


<img src="images/invite-candidate.png" style="margin:auto; display:block; width: 600px; max-width: 80vw;" alt="Candidate HulkHire Interview Invite"/>

<!-- ![Candidate HulkHire Interview Invite](images/invite-candidate.png) -->


This will send an interview invite to candidates. Candidate can chose any interview slots, And appear for the interview.


**How to monitor interview updates in Lever**  
Just after scheduling the interview, You will see an interview panel is created for the candidate.


<img src="images/interview-panel.png" style="margin:auto; display:block; width: 600px; max-width: 80vw;" alt="HulkHire Interview Panel"/>

<!-- ![HulkHire Interview Panel](images/interview-panel.png) -->


You can view this panel, to get the current status of the candidate.
Interview time, skill set, Link to candidate profile and results page all are available in the panel

All interview updates like candidate interview scheduled, rescheduled, cancelled etc can be tracked in Notes section as well.

<img src="images/interview-updates.png" style="margin:auto; display:block; width: 600px; max-width: 80vw;" alt="HulkHire Interview updates"/>


**Feedback of candidates**  
Once the candidate interview is completed and Feeedback is ready.
You can see full detailed report of candidate on feedback for section

<img src="images/interview-feedback.png" style="margin:auto; display:block; width: 600px; max-width: 80vw;" alt="HulkHire Interview feedback"/>

<!-- ![HulkHire Interview feedback](images/interview-feedback.png) -->


All fields like Over all Score, score report, Observations, projects, communication, excitement to join. Full detailed report would be available here.

<img src="images/feedback-report.png" style="margin:auto; display:block; width: 600px; max-width: 80vw;" alt="HulkHire Interview 360 degree Feedback"/>



Apart from this, In notes section , we will share a link to candidate details page and feedback page. 
You can use this link to view candidate video, questions that were asked during interview and his responses.

<img src="images/feedback-link.png" style="margin:auto; display:block; width: 600px; max-width: 80vw;" alt="HulkHire Interview Feedback Link"/>


**Tags**  
We understand reporting and workflows are important for you. So are for us.
To generate any kind of report either on score or staus. We add relevant tags to candidates profile.

Tags used to mark candidate score are - 

| TAG         | SCORE       |
| ----------- | ----------- |
| HULKHIRE: BRONZE Medalist   | Score>=60 and Score < 75       |
| HULKHIRE: SILVER Medalist   | Score >= 75 and Score < 90 |
| HULKHIRE: GOLD Medalist   | Score 90 and above           |


Tags used to mark candidate interview status are -   

| TAG         | Description       |
| ----------- | ----------- |
| HULKHIRE:INTERVIEW INVITED  | This tag denotes that we have contacted the candidate to book his interview slot   |
| HULKHIRE:INTERVIEW SCHEDULED   | This tag denotes that candidate has booked his interview slot   |
| HULKHIRE:INTERVIEW COMPLETED  | This tag denotes that interview is successfully completed with candidate  |
| HULKHIRE:INTERVIEW CANCELLED  | This tag denotes that interview is cancelled with candidate. It could happen because of multiple reasons.  |
  
<br>

You could see tags associated with candidate in Lever on  Candidate Page.

<br>
 
<img src="images/feedback-tags.png" style="margin:auto; display:block; width: 600px; max-width: 80vw;" alt="HulkHire Connect"/>


These tags you can use to filter out the data in your workflows and reports.
You can also use candidate feedback Score field in your workflows to filter more specific candidates




**Troubleshooting**  
We do not expect that you would get into any kind of issues. As we have taken care of worst possible scenarios.
However, we something goes wrong,   

1. Login to HulkHire and Go to **Admin > Integrations**  
2. Look for **Lever** and click on **Reconnect**  
3. This will reset all settings again    



<img src="images/hulkhire-reconnect.png" style="margin:auto; display:block; width: 600px; max-width: 80vw;" alt="HulkHire Connect"/>


**Support**  
We love to talk to you, hear about your experiences and like to know what we could do better to make your life more pleasant.

Just drop an one liner email on [care@hulkhire.com](mailto:care@hulkhire.com) and we will get in touch

​




